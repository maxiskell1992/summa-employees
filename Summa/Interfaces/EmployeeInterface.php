<?php

namespace Summa\Interfaces;

interface EmployeeInterface
{
    /**
     * Asigna el id al empleado.
     *
     * @param  integer $newId
     * @return void
     */
    public function setId($newId);

    /**
     * Asigna un nombre al empleado.
     *
     * @param  string $newFirstName
     * @return void
     */
    public function setFirstName($newFirstName);

    /**
     * Asigna un apellido al empleado.
     *
     * @param  string $newLastName
     * @return void
     */
    public function setLastName($newLastName);

    /**
     * Asigna la edad del empleado.
     *
     * @param  integer $newAge
     * @return void
     */
    public function setAge($newAge);

    /**
     * Devuelve el id del empleado.
     *
     * @return integer
     */
    public function getId();

    /**
     * Devuelve el nombre del empleado.
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Devuelve el apellido del empleado.
     *
     * @return string
     */
    public function getLastName();

    /**
     * Devuelve la edad del empleado.
     *
     * @return integer
     */
    public function getAge();
}
