<?php

namespace Summa;

use Summa\Interfaces\EmployeeInterface;

class Company
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * Lista de empleados de la empresa.
     *
     * @var array
     */
    protected $employees;

    /**
     * Instancia un nuevo Company.
     *
     * @param integer $id
     * @param string  $name
     */
    public function __construct($id, $name)
    {
        $this->setId($id);
        $this->setName($name);
    }

    /**
     * Asigna el id de la empresa.
     *
     * @param  integer  $newId
     * @return void
     */
    public function setId($newId)
    {
        $this->id = $newId;
    }

    /**
     * Asigna el nombre de la empresa.
     *
     * @param  string $name
     * @return void
     */
    public function setName($newName)
    {
        $this->name = $newName;
    }

    /**
     * Devuelve el Id de la empresa.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Devuelve el nombre de la empresa.
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Agrega un nuevo registro de empleado.
     *
     * @param  EmployeeInterface $employee
     * @return void;
     */
    public function addEmployee(EmployeeInterface $employee)
    {
        $this->employees[$employee->getId()] = $employee;
    }

    /**
     * Agrega varios nuevos registros empleados.
     *
     * @param  array of EmployeeInteface $employees
     * @return void
     */
    public function addEmployeesBatch(array $employees)
    {
        foreach ($employees as $employee) {
            $this->addEmployee($employee);
        }
    }

    /**
     * Devuelve la lista de todos los empleados.
     *
     * @return array of Employee
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Devuelve el Employee correspondiente a $id.
     *
     * @return Employee
     */
    public function getEmployee($id)
    {
        $employes = $this->getEmployees();

        if ( ! array_key_exists($employee->id, $this->getEmployes())) {
            throw new Exception('El id dado no corresponde con ningun empleado de la empresa');
        }

        return $this->employees[$employee->id];
    }

    /**
     * Devuelve el promedio de edad de todos los empleados de la empresa.
     *
     * @return integer
     */
    public function getEmployeesAverageAge()
    {
        $employees = $this->getEmployees();
        $qty = count($employees);
        $totalAge = 0;

        if ($qty === 0) {
            throw new Exception('La empresa no tiene empleados');
        }

        foreach ($employees as $employee) {
            $totalAge += $employee->getAge();
        }

        return $totalAge / $qty;
    }
}
