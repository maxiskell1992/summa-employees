<?php

namespace Summa\Employees;

use Summa\Interfaces\EmployeeInterface;

class Designer extends Employee implements EmployeeInterface
{
    /**
     * @var integer
     */
    private $typeId;

    /**
     * Instancia un nuevo Designer.
     *
     * @param integer $id
     * @param string  $firsName
     * @param string  $lastName
     * @param integer $age
     * @param integer $typeId
     */
    public function __construct($id, $firstName, $lastName, $age, $typeId)
    {
        parent::__construct($id, $firstName, $lastName, $age);
        $this->setTypeId($typeId);
    }

    /**
     * Asigna el id del tipo correspondiente al diseñador.
     *
     * @param  integer $newTypeId
     * @return void
     */
    public function setTypeId($newTypeId)
    {
        $this->typeId = $newTypeId;
    }

    /**
     * Devuelve el id del tipo correspondiente al diseñador.
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }
}

