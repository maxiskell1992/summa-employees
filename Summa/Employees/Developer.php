<?php

namespace Summa\Employees;

use Summa\Interfaces\EmployeeInterface;

class Developer extends Employee implements EmployeeInterface
{
    /**
     * @var integer
     */
    private $languageId;

    /**
     * Instancia un nuevo Developer.
     *
     * @param integer $id
     * @param string  $firsName
     * @param string  $lastName
     * @param integer $age
     * @param integer $languageId
     */
    public function __construct($id, $firstName, $lastName, $age, $languageId)
    {
        parent::__construct($id, $firstName, $lastName, $age);
        $this->setLanguageId($languageId);
    }

    /**
     * Asigna el id del lenguage correspondiente al Developer.
     *
     * @param  integer $id
     * @return void
     */
    public function setLanguageId($newLanguageId)
    {
        $this->languageId = $newLanguageId;
    }

    /**
     * Devuelve el id del lenguage correspondiente al Developer.
     *
     * @return integer
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }
}
