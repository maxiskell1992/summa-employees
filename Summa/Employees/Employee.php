<?php

namespace Summa\Employees;

use Summa\Interfaces\EmployeeInterface;

class Employee implements EmployeeInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var integer
     */
    protected $age;

    /**
     * Instancia un nuevo Employee.
     *
     * @param integer $id
     * @param string  $firsName
     * @param string  $lastName
     * @param integer $age
     */
    public function __construct($id, $firstName, $lastName, $age)
    {
        $this->setId($id);
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->setAge($age);
    }

    /**
     * Asigna el id al empleado.
     *
     * @param  integer $newId
     * @return void
     */
    public function setId($newId)
    {
        $this->id = $newId;
    }

    /**
     * Asigna un nombre al empleado.
     *
     * @param  string $newFirstName
     * @return void
     */
    public function setFirstName($newFirstName)
    {
        $this->firstName = $newFirstName;
    }

    /**
     * Asigna un apellido al empleado.
     *
     * @param  string $newLastName
     * @return void
     */
    public function setLastName($newLastName)
    {
        $this->lastName = $newLastName;
    }

    /**
     * Asigna la edad del empleado.
     *
     * @param  integer $newAge
     * @return void
     */
    public function setAge($newAge)
    {
        $this->age = $newAge;
    }

    /**
     * Devuelve el id del empleado.
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Devuelve el nombre del empleado.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Devuelve el apellido del empleado.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Devuelve la edad del empleado.
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }
}
