# Employees

Implementación del ejercicio enviado por Marina.

Para el manejo de los empleados, empleé el uso de un array como propiedad de Company, lo cual no es completamente óptimo, ya que debería ser una relación dentro de una base de datos, más que una propiedad en sí. Pero como en el ejercicio se veía presentado como la segunda, y no tengo acceso a un ORM u otra herramienta que me facilite el acceso a una base de datos (para simplicidad del ejercicio y poruqe no era un punto a evaluar), implementé la _lista de empleados_ de ésta manera.

El código es completamente funcional, y puede probarse clonando el repositorio y corriendo el ejemplo en la terminal, de la siguiente manera:

```
$ git clone https://maxiskell@bitbucket.org/maxiskell/summa-employees.git summa-employees
$ cd summa-employees
$ php example
```
